<?php

namespace App\Controller;

use App\Entity\Classe;
use App\Entity\Cours;
use App\Entity\Matiere;
use App\Form\CoursType;

use Doctrine\Persistence\ObjectManager;

use phpDocumentor\Reflection\DocBlock\Tags\Property;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class CoursController extends AbstractController
{



    /**
     * @Route("/cours/{id}", name="cours.edit",methods="GET|POST")
     */
    public function edit( Cours $cours ,Request $request )
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(CoursType::class, $cours);
        $form ->handleRequest($request);
        if ($form -> isSubmitted() && $form ->isValid())
        {
            $file = $form->get('brochure')->getData();
            $uploads_directory = $this ->getParameter('uploads_directory');
            $filename = md5(uniqid())  . '.' .  $file-> guessExtension();

            $file -> move (
                $uploads_directory,
                $filename,

                );

            if ($file) {

                $cours->setBrochureFilename($filename);
            };

            $em->flush();
            return $this->redirectToRoute('cours');
        }

        return $this->render('cours/edit.html.twig',[
            'cours'=>$cours,
             'form' => $form -> createView()
        ]);

    }
    /**
     * @Route("/cours", name="cours")
     */
    public function index()
    {
        $cours = $this-> getDoctrine()->getRepository(Cours::class)->findAll();
        return $this->render('cours/index.html.twig', [
            'controller_name' => 'CoursController',
            'cours'=> $cours
        ]);
    }

    /**
     * @Route("/create", name="Cours.new")
     */
    public function new (Request $request)
    {

        $cours=new cours ();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(CoursType::class, $cours);
        $form ->handleRequest($request);




        if ($form -> isSubmitted() && $form ->isValid())
        {
            $file = $form->get('brochure')->getData();
            $uploads_directory = $this ->getParameter('uploads_directory');
            $filename = md5(uniqid())  . '.' .  $file-> guessExtension();

            $file -> move (
                $uploads_directory,
                $filename,

            );

            if ($file) {

                $cours->setBrochureFilename($filename);
            };

           $em->persist($cours);
           $em->flush();
            return $this->redirectToRoute('cours');
        }
        return $this->render('cours/new.html.twig',[
            'cours'=>$cours,
            'form' => $form -> createView()
        ]);
    }

    /**
     * @Route("/cours/{id}", name="cours.delete",methods="DELETE")
     */
    public function delete(Cours $cours ,$id)
    {

      $em = $this->getDoctrine()->getManager();
      $em->remove($cours);
       $em->flush();

        return $this->redirectToRoute('cours');
    }

    /**
     * @Route("/details/{id}", name="details")
     */
    public function details($id)
    {
        $Cours = $this-> getDoctrine()->getRepository(Cours::class)->findBy(array('id'=>$id));
        return $this->render('cours/details.html.twig', [
            'controller_name' => 'CoursController',
            'Cours'=> $Cours
        ]);
    }
    /**
     * @Route("/Matiereid/{id}", name="Matiereid")
     */
    public function Matiereid($id)
    {

        $Matiereid = $this->getDoctrine()->getRepository(Cours::class)->findBy(array("matiere" => $id));
        $Matiereide = $this->getDoctrine()->getRepository(Matiere::class)->findBy(array("id" => $id));

        return $this->render('cours/detailmes.html.twig', [
            'controller_name' => 'CoursController',
            'Matiereid' => $Matiereid,
            'Matiereide'=> $Matiereide
        ]);
    }
}
