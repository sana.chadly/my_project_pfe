<?php

namespace App\Controller;

use App\Entity\Cours;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EnseignantController extends AbstractController
{
    /**
     * @Route("/enseignant", name="enseignant")
     */
    public function index()
    {
        return $this->render('enseignant/index.html.twig', [
            'controller_name' => 'EnseignantController',
        ]);
    }
    /**
     * @Route("/details/{id}", name="details")
     */
    public function details($id)
    {
        $Cours = $this-> getDoctrine()->getRepository(Cours::class)->findBy(array('id'=>$id));
        return $this->render('cours/details.html.twig', [
            'controller_name' => 'CoursController',
            'Cours'=> $Cours
        ]);
    }

}
