<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CallendarController extends AbstractController
{
    /**
     * @Route("/callendar", name="callendar")
     */
    public function index()
    {
        return $this->render('callendar/index.html.twig', [
            'controller_name' => 'CallendarController',
        ]);
    }
}
