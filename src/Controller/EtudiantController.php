<?php

namespace App\Controller;

use App\Entity\Etudiant;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class EtudiantController extends AbstractController
{
    /**
     * @Route("/etudiant", name="etudiant")
     */
    public function index()
    {
        $etudiant = $this->getDoctrine()->getManager()->getRepository(Etudiant::class)->find($this->getUser());
        return $this->render('etudiant/index.html.twig', [
            'etudiant' => $etudiant,
        ]);
    }
}
