<?php

namespace App\Controller;

use App\Entity\Notes;
use Proxies\__CG__\App\Entity\Anneeuni;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MatiereController extends AbstractController
{
    /**
     * @Route("/matiere", name="matiere")
     */
    public function index()
    {

        return $this->render('matiere/index.html.twig', [
            'controller_name' => 'MatiereController',

        ]);
    }


}
