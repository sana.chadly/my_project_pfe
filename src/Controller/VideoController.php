<?php


namespace App\Controller;

use App\Entity\Classe;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;


class VideoController extends AbstractController
{
    /**
     * @Route("/classe_virtuelle", name="classe_virtuelle")
     */
    public function index(Request $request)
    {
        $user = $this->getUser();
        $nomClasse = "";
        $classes = null;
        $test = true;
        if ($this->isGranted('ROLE_Etudiant')) {
            $nomClasse = $user->getClasse()->getNameClasse();
        } else {
            $test = false;
            $classes = $user->getClasses();
            foreach ($classes as $classe) {
                if ($classe->getId() == $request->get("classe")) {
                    $nomClasse = $classe->getNameClasse();
                    $test = true;
                }
            }
        }


        return $this->render('Video/video.html.twig', [
            'controller_name' => 'VideoController',
            "nomClasse" => $nomClasse,
            "classes" => $classes,
            "test" => $test

        ]);
    }


}