<?php

namespace App\Controller;

use App\Entity\Evenement;
use App\Entity\Reservation;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class EvenementController extends AbstractController
{
    /**
     * @Route("/evenement", name="evenement")
     */
    public function index(Request $request)
    {

        $evenement = $this->getDoctrine()->getRepository(Evenement::class)->findNewEvents();
        $em = $this->getDoctrine()->getManager();


        return $this->render('evenement/index.html.twig', [
            'controller_name' => 'EvenementController',
            'evenement' => $evenement
        ]);
    }

    /**
     * @Route("/participation/{id}", name="participation",methods="GET|POST")
     */
    public function res($id)
    {
        $em = $this->getDoctrine()->getManager();

        $Reservation = new Reservation();

        $evenement = $this->getDoctrine()->getRepository(Evenement::class)->find($id);
        $Reservation->setEvenement($evenement);

        $User = $this->getUser();
        $Reservation->setUser($User);


        $evenement->setNbrePart($evenement->getNbrePart() + 1);

        $em->persist($Reservation);
        $em->flush();

        return $this->redirectToRoute('evenement');

    }

    /**
     * @Route("/check/{uid}/{eid}", name="part")
     */
    public function part($uid, $eid)
    {

        $paticipation = $this->getDoctrine()->getRepository(Reservation::class)->findBy(array("User" => $uid, "Evenement" => $eid));

        if ($paticipation == null) {
            return new Response("false");
        } else return new Response("true");
    }

    /**
     * @Route("/egale/{id}", name="egale")
     */
    public function egale($id)
    {

        $evenement = $this->getDoctrine()->getRepository(Evenement::class)->find($id);

        if ($evenement->getNbrePart() == $evenement->getNbrePlace()) {
            return new Response("false");
        } else return new Response("true");
    }

    /**
     * @Route("/eventpart/{uid}", name="eventpart")
     */
    public function eventpart($uid)
    {

        $eventpart = $this->getDoctrine()->getRepository(Reservation::class)->findBy(array("User" => $uid));


        return $this->render('evenement/eventpart.html.twig', [
            'controller_name' => 'EvenementController',
            'eventpart' => $eventpart
        ]);
    }

    /**
     * @Route("/rechevent", name="rechevent")
     */
    public function rechercheaction(Request $request)
    {
        $evenements = $this->getDoctrine()->getRepository(Evenement::class)->findAll();
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')) {
            $nom = $request->get('nom');
            $evenements = $em->getRepository(Evenement::class)->findBy(array("nom" => $nom));

        }
        return $this->render('evenement/rech.html.twig', [
            'controller_name' => 'EvenementController',
            'evenements' => $evenements

        ]);
    }

    /**
     * @Route("/date/{id}", name="date")
     */
    public function date($id)
    {

        $evenement = $this->getDoctrine()->getRepository(Evenement::class)->find($id);

        if ($evenement->getDateFin() < new \DateTime("now")) {

            return new Response("true");
        }
        return new Response("false");
    }

    /**
     * @Route("/search/{search}", name="search")
     */
    public function search(Request $request,$search)
    {

        if (!$request->isXmlHttpRequest()) {
            return $this->render("evenement/index.html.twig");
        }

        if (!$searchTerm = trim($request->query->get("search", $search))) {
            return new JsonResponse(["error" => "Search term not specified."], Response::HTTP_BAD_REQUEST);
        }
        $em = $this->getDoctrine()->getManager();


        if (!($results = $em->getRepository(Evenement::class)->findEntitiesByString($searchTerm))) {
            return new JsonResponse(["error" => "Aucun évenement trouvé"], Response::HTTP_NOT_FOUND);
        }

        return new JsonResponse([
            "html" => $this->renderView("evenement/rech.html.twig", ["evenement" => $results]),
        ]);
    }

}
