<?php

namespace App\Controller;

use App\Entity\Absence;
use App\Entity\Classe;
use App\Entity\Etudiant;
use App\Form\AbsenceType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class ClasseController extends AbstractController
{
    /**
     * @Route("/classe/{id}", name="classe")
     */
    public function index($id,Request $request)
    {

        $Etudiant = $this-> getDoctrine()->getRepository(Etudiant::class)->findBy(array("classe"=>$id));
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')){
            $nom= $request->get('nom');
            $Etudiant= $em->getRepository(Etudiant::class)->findBy(array ("nom"=>$nom));

        }
        $Classe = $this-> getDoctrine()->getRepository(Classe::class)->findBy(array('id' => $id));
        return $this->render('classe/index.html.twig', [
            'controller_name' => 'ClasseController',
            'Etudiant'=>$Etudiant ,
            'Classe'=>$Classe
        ]);
    }
    /**
     * @Route("/createAbsence/{id}/{classeId}", name="Absence.new")
     */
    public function new(Request $request ,$id)
    {

        $Absences = $this-> getDoctrine()->getRepository(Etudiant::class)->find($id);
        $Ab = $this-> getDoctrine()->getRepository(Etudiant::class)->findBy(array('id' => $id));

        $Absence = new Absence();
        $Absence->setEtudiant($Absences);
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(AbsenceType::class, $Absence);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em->persist($Absence);
            $em->flush();
            return $this->redirectToRoute('classe',array("id"=>$request->get("classeId")));
        }
        return $this->render('classe/new.html.twig', [
            'Absence' => $Absences,
            'Ab' => $Ab,
            'form' => $form->createView()
        ]);
    }
    /**
     * @Route("/absenced/{id}", name="absences")
     */
    public function indexx($id)
    {

        $Absence = $this->getDoctrine()->getRepository(Absence::class)->findBy(array("etudiant" => $id));
        return $this->render('absence/index.html.twig', [
            'controller_name' => 'classe/new.html.twig',
            'Absence' => $Absence
        ]);
    }
}
