<?php

namespace App\Controller;


use App\Entity\Classe;
use App\Entity\Emploi;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\Routing\Annotation\Route;

class EmploiController extends AbstractController
{
    /**
     * @Route("/emploi", name="emploi")
     */
    public function index()
    {
        $cours = $this->getDoctrine()->getRepository(Emploi::class)->findAll();
        return $this->render('emploi/index.html.twig', [
            'controller_name' => 'EmploiController',
            'Emploi' => $cours
        ]);

    }

    /**
     * @Route("/emploiid/{id}", name="emploiid")
     */
    public function emploiid($id)
    {
        $emploiid = $this->getDoctrine()->getRepository(Emploi::class)->findBy(array("mploie" => $id));
        $classeide = $this->getDoctrine()->getRepository(Classe::class)->findBy(array("id" => $id));
        $enseignantide = $this->getDoctrine()->getRepository(Emploi::class)->findBy(array("emploi" => $id));
        return new JsonResponse([
            "html" => $this->renderView("emploi/emploiid.html.twig", ["emploiid" => $emploiid, "enseignantide" => $enseignantide, "classeide" => $classeide]),
        ]);
    }
}
