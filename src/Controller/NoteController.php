<?php

namespace App\Controller;

use App\Entity\Matiere;
use App\Entity\Notes;
use App\Form\NotesType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class NoteController extends AbstractController
{
    /**
     * @Route("/note", name="note")
     */
    public function index()
    {
        $em = $this->getDoctrine()->getManager();

        $Notes = $this->getDoctrine()->getRepository(Notes::class)->findAll();

        return $this->render('note/index.html.twig', [
            'controller_name' => 'NoteController',
            'Notes' => $Notes,

        ]);
    }

    /**
     * @Route("/notes/{id}", name="notes.edit",methods="GET|POST")
     */
    public function edit(Notes $notes, Request $request)
    {
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(NotesType::class, $notes);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $em->flush();
            return $this->redirectToRoute('note');
        }

        return $this->render('note/edit.html.twig', [
            '$notes' => $notes,
            'form' => $form->createView()
        ]);

    }

    /**
     * @Route("/creat", name="notes.new")
     */
    public function new(Request $request)
    {


        $notes = new Notes();
        $em = $this->getDoctrine()->getManager();
        $form = $this->createForm(NotesType::class, $notes);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $moyenne = ($notes->getNotecc() * 0.2 + $notes->getNotetp() * 0.3 + $notes->getNoteexamen() * 0.5);
            $notes->setMoyenne($moyenne);
            $em->persist($notes);
            $em->flush();
            return $this->redirectToRoute('note');
        }
        return $this->render('note/new.html.twig', [
            'notes' => $notes,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/notes/{id}", name="notes.delete",methods="DELETE")
     */
    public function delete(Notes $notes, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($notes);
        $em->flush();

        return $this->redirectToRoute('note');
    }

    /**
     * @Route("/notenotetest", name="notenote")
     */
    public function notesnotes(Request $request)
    {
        $doctrine = $this->getDoctrine();
        if ($request->get("annee") != null) {
            $Notespa = $doctrine->getRepository(Notes::class)->findAnneeByUser($this->getUser());
            $Notes = $doctrine->getRepository(Notes::class)->findBy(array("anneeuni" => $request->get("annee"), "Etudiant" => $this->getUser()));

            $moy = 0;
            $coefs = 0;
            foreach ($Notes as $note) {
                $moy += $note->getMoyenne() * $note->getMatiere()->getCoef();
                $coefs += $note->getMatiere()->getCoef();
            }
            $moy = $moy / $coefs;
            return $this->render('note/notenote.html.twig', [
                'controller_name' => 'NoteController',
                "Notes" => $Notes, "moyg" => $moy, "Notespa" => $Notespa
            ]);
        }
        $Notespa = $doctrine->getRepository(Notes::class)->findAnneeByUser($this->getUser());
        return $this->render('note/notenote.html.twig', [
            'controller_name' => 'NoteController',
            "Notespa" => $Notespa, "moyg" => null, "Notes" => null
        ]);


    }

}
