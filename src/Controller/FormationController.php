<?php

namespace App\Controller;


use App\Entity\Formation;
use App\Entity\Niveau;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class FormationController extends AbstractController
{
    /**
     * @Route("/formation", name="formation")
     */
    public function index($id , Request $request)
    {
        $formation = $this-> getDoctrine()->getRepository(Formation::class)->findBy(array("niveau"=>$id));
        $Niveau = $this-> getDoctrine()->getRepository(Niveau::class)->findBy(array('id' => $id));
        $em = $this->getDoctrine()->getManager();
        if ($request->isMethod('POST')){
            $name= $request->get('name');
            $formation= $em->getRepository(Formation::class)->findBy(array ("name"=>$name));

        }
        return $this->render('formation/index.html.twig', [
            'controller_name' => 'FormationController',
            'formation'=>$formation ,
            'Niveau'=>$Niveau
        ]);
    }
    /**
     * @Route("/Formationide/{id}", name="Formationide")
     */
    public function Formationid($id)
    {


        $Formationide = $this->getDoctrine()->getRepository(Formation::class)->findBy(array("id" => $id));

        return $this->render('formation/detailf.html.twig', [
            'controller_name' => 'FormationController',
            'Formationide'=> $Formationide
        ]);
    }
}
