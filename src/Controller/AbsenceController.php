<?php

namespace App\Controller;

use App\Entity\Absence;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class AbsenceController extends AbstractController
{
    /**
     * @Route("/absenced/{id}", name="absences")
     */
    public function index($id)
    {

        $Absence = $this->getDoctrine()->getRepository(Absence::class)->findBy(array("etudiant" => $id));
        return $this->render('absence/index.html.twig', [
            'controller_name' => 'AbsenceController',
            'Absence' => $Absence
        ]);
    }


    /**
     * @Route("/Absences/{id}", name="Absence.delete",methods="DELETE")
     */
    public function delete(Absence $Absence, $id)
    {

        $em = $this->getDoctrine()->getManager();
        $em->remove($Absence);
        $em->flush();

        return $this->redirectToRoute('absence');
    }

    /**
     * @Route("/absenceslist/{id}", name="absences.list")
     */
    public function absence($id)
    {

        $Absencee = $this->getDoctrine()->getRepository(Absence::class)->findBy(array("etudiant" => $id,"Etat"=>"Absent"));
        return new JsonResponse([
            "html" => $this->renderView("absence/index.html.twig", ["Absence" => $Absencee]),
        ]);

    }

   

}
