<?php

namespace App\Controller;

use App\Service\MailerService;
use EasyCorp\Bundle\EasyAdminBundle\Controller\EasyAdminController;
use EasyCorp\Bundle\EasyAdminBundle\Event\EasyAdminEvents;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

class AdminController extends EasyAdminController
{
    protected $config;
    protected $entity;
    protected $request;
    protected $em;


    private $encoder;
    private $maile;

    public function __construct(UserPasswordEncoderInterface $encoder, MailerService $maile)
    {
//        parent::__construct($encoder, $maile);
        $this->encoder = $encoder;
        $this->maile = $maile;
    }

    /**
     * @Route("/admin", name="admin")
     */
    protected function newAction()
    {

        $pass = (new \App\Utils\ProjectTools)->generationCode();


        $this->dispatch(EasyAdminEvents::PRE_NEW);

        $entity = $this->executeDynamicMethod('createNew<EntityName>Entity');

        $easyadmin = $this->request->attributes->get('easyadmin');
        $easyadmin['item'] = $entity;
        $this->request->attributes->set('easyadmin', $easyadmin);

        $fields = $this->entity['new']['fields'];

        $newForm = $this->executeDynamicMethod('create<EntityName>NewForm', [$entity, $fields]);

        $newForm->handleRequest($this->request);
        if ($newForm->isSubmitted() && $newForm->isValid()) {
            $this->processUploadedFiles($newForm);
            $var = $newForm->getname();
            if (($var == 'admin') or ($var == 'etudiant') or ($var == 'enseignant')) {
                $passe = $this->encoder->encodePassword($entity, $pass);
                $name = $entity->getNom() . $entity->getPrenom();
                $mailto = $entity->getEmail();//mail
                $entity->setPassword($passe);//generer un mot pass

                if ($this->entity['class'] == "App\Entity\Admin") {

                    $username = $name . "Admin@universite.com";
                    $entity->setRoles('ROLE_ADMIN');
                    $entity->setUsername($username);
                    $this->maile->sendMessage($name, $mailto, $pass, $username);
                }
                if ($this->entity['class'] == "App\Entity\Etudiant") {
                    $username = $name . "Etudiant@universite.com";
                    $entity->setRoles('ROLE_Etudiant');
                    $entity->setUsername($username);
                    $this->maile->sendMessage($name, $mailto, $pass, $username);
                }
                if ($this->entity['class'] == "App\Entity\Enseignant") {
                    $username = $name . "Enseignant@universite.com";
                    $entity->setRoles('ROLE_Enseignant');
                    $entity->setUsername($username);
                    $this->maile->sendMessage($name, $mailto, $pass, $username);
                }
            }


            $this->dispatch(EasyAdminEvents::PRE_PERSIST, ['entity' => $entity]);
            $this->executeDynamicMethod('persist<EntityName>Entity', [$entity, $newForm]);
            $this->dispatch(EasyAdminEvents::POST_PERSIST, ['entity' => $entity]);


            return $this->redirectToReferrer();
        }

        $this->dispatch(EasyAdminEvents::POST_NEW, [
            'entity_fields' => $fields,
            'form' => $newForm,
            'entity' => $entity,
        ]);


        $parameters = [
            'form' => $newForm->createView(),
            'entity_fields' => $fields,
            'entity' => $entity,
        ];


        return $this->executeDynamicMethod('render<EntityName>Template', ['new', $this->entity['templates']['new'], $parameters]);


    }

    /**
     * The method that is executed when the user performs a 'list' action on an entity.
     *
     * @return Response
     */
    protected function listAction()
    {
        $this->dispatch(EasyAdminEvents::PRE_LIST);

        $fields = $this->entity['list']['fields'];
        $paginator = $this->findAll($this->entity['class'], $this->request->query->get('page', 1), $this->entity['list']['max_results'], $this->request->query->get('sortField'), $this->request->query->get('sortDirection'), $this->entity['list']['dql_filter']);

        $this->dispatch(EasyAdminEvents::POST_LIST, ['paginator' => $paginator]);

        $parameters = [
            'paginator' => $paginator,
            'fields' => $fields,
            'batch_form' => $this->createBatchForm($this->entity['name'])->createView(),
            'delete_form_template' => $this->createDeleteForm($this->entity['name'], '__id__')->createView(),
        ];

        return $this->executeDynamicMethod('render<EntityName>Template', ['list', $this->entity['templates']['list'], $parameters]);
    }
}