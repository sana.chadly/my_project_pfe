<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\MatiereRepository")
 */
class Matiere
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_matiere;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbr_h;


    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Classe", inversedBy="matieres")
     * @ORM\JoinTable(name="matiere_classe")
     */
    private $Classe;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Enseignant", mappedBy="Matiere")
     */
    private $enseignants;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Cours", mappedBy="matiere")
     */
    private $cours;

    /**
     * @ORM\Column(type="float")
     */
    private $Coef;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notes", mappedBy="Matiere")
     */
    private $notes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Absence", mappedBy="relation")
     */
    private $absences;






    public function __construct()
    {
        $this->enseignant = new ArrayCollection();
        $this->Classe = new ArrayCollection();
        $this->enseignants = new ArrayCollection();
        $this->cours = new ArrayCollection();
        $this->notes = new ArrayCollection();
        $this->absences = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameMatiere(): ?string
    {
        return $this->name_matiere;
    }

    public function setNameMatiere(string $name_matiere): self
    {
        $this->name_matiere = $name_matiere;

        return $this;
    }

    public function getNbrH(): ?int
    {
        return $this->nbr_h;
    }

    public function setNbrH(int $nbr_h): self
    {
        $this->nbr_h = $nbr_h;

        return $this;
    }


    public function __toString()
    {
        return $this->name_matiere;
        
    }

    /**
     * @return Collection|Classe[]
     */
    public function getClasse(): Collection
    {
        return $this->Classe;
    }

    public function addClasse(Classe $classe): self
    {
        if (!$this->Classe->contains($classe)) {
            $this->Classe[] = $classe;
        }

        return $this;
    }

    public function removeClasse(Classe $classe): self
    {
        if ($this->Classe->contains($classe)) {
            $this->Classe->removeElement($classe);
        }

        return $this;
    }

    /**
     * @return Collection|Enseignant[]
     */
    public function getEnseignants(): Collection
    {
        return $this->enseignants;
    }

    public function addEnseignant(Enseignant $enseignant): self
    {
        if (!$this->enseignants->contains($enseignant)) {
            $this->enseignants[] = $enseignant;
            $enseignant->addMatiere($this);
        }

        return $this;
    }

    public function removeEnseignant(Enseignant $enseignant): self
    {
        if ($this->enseignants->contains($enseignant)) {
            $this->enseignants->removeElement($enseignant);
            $enseignant->removeMatiere($this);
        }

        return $this;
    }

    /**
     * @return Collection|Cours[]
     */
    public function getCours(): Collection
    {
        return $this->cours;
    }

    public function addCours(Cours $cours): self
    {
        if (!$this->cours->contains($cours)) {
            $this->cours[] = $cours;
            $cours->setMatiere($this);
        }

        return $this;
    }

    public function removeCours(Cours $cours): self
    {
        if ($this->cours->contains($cours)) {
            $this->cours->removeElement($cours);
            // set the owning side to null (unless already changed)
            if ($cours->getMatiere() === $this) {
                $cours->setMatiere(null);
            }
        }

        return $this;
    }

    public function getCoef(): ?float
    {
        return $this->Coef;
    }

    public function setCoef(float $Coef): self
    {
        $this->Coef = $Coef;

        return $this;
    }

    /**
     * @return Collection|Notes[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Notes $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setMatiere($this);
        }

        return $this;
    }

    public function removeNote(Notes $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
            // set the owning side to null (unless already changed)
            if ($note->getMatiere() === $this) {
                $note->setMatiere(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Absence[]
     */
    public function getAbsences(): Collection
    {
        return $this->absences;
    }

    public function addAbsence(Absence $absence): self
    {
        if (!$this->absences->contains($absence)) {
            $this->absences[] = $absence;
            $absence->setRelation($this);
        }

        return $this;
    }

    public function removeAbsence(Absence $absence): self
    {
        if ($this->absences->contains($absence)) {
            $this->absences->removeElement($absence);
            // set the owning side to null (unless already changed)
            if ($absence->getRelation() === $this) {
                $absence->setRelation(null);
            }
        }

        return $this;
    }







}
