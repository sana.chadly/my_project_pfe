<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use App\Entity\User ;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EtudiantRepository")
 */

class Etudiant extends User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @ORM\Column(type="json")
     */
    protected $roles;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Classe", inversedBy="etudiant")
     */
    private $classe;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notes", mappedBy="etudiant")
     */
    private $notes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Anneeuni", mappedBy="etudiant")
     */
    private $anneeunis;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Absence", mappedBy="etudiant")
     */
    private $absences;

    public function __construct()
    {
        parent::__construct();
        $this->notes = new ArrayCollection();
        $this->anneeunis = new ArrayCollection();
        $this->absences = new ArrayCollection();
    }



    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_Etudiant';
        return array_unique($roles);


    }

    /**
     * @param array $roles
     */
    public function setRoles($roles): void
    {
        $this->roles[] = $roles;
    }

    public function __toString()
    {
        return $this->getUsername();
        // TODO: Implement __toString() method.
    }

    public function getClasse(): ?Classe
    {
        return $this->classe;
    }

    public function setClasse(?Classe $classe): self
    {
        $this->classe = $classe;

        return $this;
    }

    /**
     * @return Collection|Notes[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Notes $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setEtudiant($this);
        }

        return $this;
    }

    public function removeNote(Notes $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
            // set the owning side to null (unless already changed)
            if ($note->getEtudiant() === $this) {
                $note->setEtudiant(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Anneeuni[]
     */
    public function getAnneeunis(): Collection
    {
        return $this->anneeunis;
    }

    public function addAnneeuni(Anneeuni $anneeuni): self
    {
        if (!$this->anneeunis->contains($anneeuni)) {
            $this->anneeunis[] = $anneeuni;
            $anneeuni->addEtudiant($this);
        }

        return $this;
    }

    public function removeAnneeuni(Anneeuni $anneeuni): self
    {
        if ($this->anneeunis->contains($anneeuni)) {
            $this->anneeunis->removeElement($anneeuni);
            $anneeuni->removeEtudiant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Absence[]
     */
    public function getAbsences(): Collection
    {
        return $this->absences;
    }

    public function addAbsence(Absence $absence): self
    {
        if (!$this->absences->contains($absence)) {
            $this->absences[] = $absence;
            $absence->setEtudiant($this);
        }

        return $this;
    }

    public function removeAbsence(Absence $absence): self
    {
        if ($this->absences->contains($absence)) {
            $this->absences->removeElement($absence);
            // set the owning side to null (unless already changed)
            if ($absence->getEtudiant() === $this) {
                $absence->setEtudiant(null);
            }
        }

        return $this;
    }



}
