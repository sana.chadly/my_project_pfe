<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EnseignantRepository")
 */
class Enseignant extends User
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @ORM\Column(type="json")
     */
    protected $roles;



    /**
     * @ORM\Column(type="array")
     */
    private $grade = [];



    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Classe", mappedBy="Enseignant")
     */
    private $classes;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Matiere", inversedBy="enseignants")
     */
    private $Matiere;




    public function __construct()
    {
        $this->classes = new ArrayCollection();
        $this->Matiere = new ArrayCollection();

    }
    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_Enseignant';
        return array_unique($roles);


    }
    /**
     * @param array $roles
     */
    public function setRoles($roles): void
    {
        $this->roles [] = $roles;
    }


    /**
     * Get grade
     *
     * @return array
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * @param array $grade
     */
    public function setGrade(array $grade): void
    {
        $this->grade = $grade;
    }




    public function __toString()
    {
        return $this->getUsername();
        // TODO: Implement __toString() method.
    }


    /**
     * @return Collection|Classe[]
     */
    public function getClasses(): Collection
    {
        return $this->classes;
    }

    public function addClass(Classe $class): self
    {
        if (!$this->classes->contains($class)) {
            $this->classes[] = $class;
            $class->addEnseignant($this);
        }

        return $this;
    }

    public function removeClass(Classe $class): self
    {
        if ($this->classes->contains($class)) {
            $this->classes->removeElement($class);
            $class->removeEnseignant($this);
        }

        return $this;
    }

    /**
     * @return Collection|Matiere[]
     */
    public function getMatiere(): Collection
    {
        return $this->Matiere;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->Matiere->contains($matiere)) {
            $this->Matiere[] = $matiere;
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->Matiere->contains($matiere)) {
            $this->Matiere->removeElement($matiere);
        }

        return $this;
    }




}

