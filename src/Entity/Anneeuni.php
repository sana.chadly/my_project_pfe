<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AnneeuniRepository")
 */
class Anneeuni
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="integer")
     */
    private $annee;



    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Noman;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Etudiant", inversedBy="anneeunis")
     */
    private $etudiant;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Notes", mappedBy="anneeuni")
     */
    private $notes;



    public function __construct()
    {

        $this->etudiant = new ArrayCollection();
        $this->notes = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAnnee(): ?int
    {
        return $this->annee;
    }

    public function setAnnee(int $annee): self
    {
        $this->annee = $annee;

        return $this;
    }


    public function getNoman(): ?string
    {
        return $this->Noman;
    }

    public function setNoman(string $Noman): self
    {
        $this->Noman = $Noman;

        return $this;
    }
    public function __toString()
    {
        return $this->getNoman();
        // TODO: Implement __toString() method.
    }

    /**
     * @return Collection|Etudiant[]
     */
    public function getEtudiant(): Collection
    {
        return $this->etudiant;
    }

    public function addEtudiant(Etudiant $etudiant): self
    {
        if (!$this->etudiant->contains($etudiant)) {
            $this->etudiant[] = $etudiant;
        }

        return $this;
    }

    public function removeEtudiant(Etudiant $etudiant): self
    {
        if ($this->etudiant->contains($etudiant)) {
            $this->etudiant->removeElement($etudiant);
        }

        return $this;
    }
    /**
     * @return Collection|Notes[]
     */
    public function getNotes(): Collection
    {
        return $this->notes;
    }

    public function addNote(Notes $note): self
    {
        if (!$this->notes->contains($note)) {
            $this->notes[] = $note;
            $note->setAnneeuni($this);
        }

        return $this;
    }

    public function removeNote(Notes $note): self
    {
        if ($this->notes->contains($note)) {
            $this->notes->removeElement($note);
            // set the owning side to null (unless already changed)
            if ($note->getAnneeuni() === $this) {
                $note->setAnneeuni(null);
            }
        }

        return $this;
    }

}
