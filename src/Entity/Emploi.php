<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\EmploiRepository")
 */
class Emploi
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Nom_Emploi;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $Date;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Classe", inversedBy="emplois")
     */
    private $mploie;


    /**
     * @ORM\Column(type="datetime")
     */
    private $createdAt;
    /**
     * @ORM\Column(type="datetime")
     */
    private $updatedAt;

    /**
     * @ORM\OneToOne(targetEntity="App\Entity\Enseignant", cascade={"persist", "remove"})
     */
    private $emploi;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        if ($this->image == "")
            return $this->image;
        else
            return 'uploads/' . basename($this->image);

    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = basename($image);
    }

    public function getNomEmploi(): ?string
    {
        return $this->Nom_Emploi;
    }

    public function setNomEmploi(string $Nom_Emploi): self
    {
        $this->Nom_Emploi = $Nom_Emploi;

        return $this;
    }

    public function getDate(): ?string
    {
        return $this->Date;
    }

    public function setDate(string $Date): self
    {
        $this->Date = $Date;

        return $this;
    }

    public function getMploie(): ?Classe
    {
        return $this->mploie;
    }

    public function setMploie(?Classe $mploie): self
    {
        $this->mploie = $mploie;

        return $this;
    }

    public function __construct()
    {

        $this->createdAt = new \DateTime();
        $this->updatedAt = new \DateTime();


    }


    /**
     * @return \DateTime
     */
    public function getCreatedAt(): \DateTime
    {
        return $this->createdAt;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt(\DateTime $createdAt): void
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getUpdatedAt(): \DateTime
    {
        return $this->updatedAt;
    }

    /**
     * @param \DateTime $updatedAt
     */
    public function setUpdatedAt(\DateTime $updatedAt): void
    {
        $this->updatedAt = $updatedAt;
    }

    public function getEmploi(): ?Enseignant
    {
        return $this->emploi;
    }

    public function setEmploi(?Enseignant $emploi): self
    {
        $this->emploi = $emploi;

        return $this;
    }
}
