<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\CoursRepository")
 */
class Cours
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_cours;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $remarque;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameCours(): ?string
    {
        return $this->name_cours;
    }

    public function setNameCours(string $name_cours): self
    {
        $this->name_cours = $name_cours;

        return $this;
    }

    public function getRemarque(): ?string
    {
        return $this->remarque;
    }

    public function setRemarque(string $remarque): self
    {
        $this->remarque = $remarque;

        return $this;
    }

    /**
     * @ORM\Column(type="string")
     */
    private $brochureFilename;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matiere", inversedBy="cours")
     */
    private $matiere;




    public function getBrochureFilename()
    {
        return $this->brochureFilename;
    }


    public function setBrochureFilename($brochureFilename)
    {

        $this->brochureFilename = $brochureFilename;

        return $this;
    }

    public function getMatiere(): ?Matiere
    {
        return $this->matiere;
    }

    public function setMatiere(?Matiere $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }





}
