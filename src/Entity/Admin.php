<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Boolean;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use phpDocumentor\Reflection\Types\Self_;

/**
 * @ORM\Entity(repositoryClass="App\Repository\AdminRepository")
 * *@ORM\InheritanceType("JOINED")
 */
class Admin extends User
{

    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @ORM\Column(type="json")
     */
    protected $roles;


    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_ADMIN';

        return $this->roles;

    }

    /**
     * @param array $roles
     */
    public function setRoles($roles): void
    {
        $this->roles[] = $roles;
    }


    public function __toString()
    {
        return $this->getUsername();
        // TODO: Implement __toString() method.
    }



}
