<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NotesRepository")
 */
class Notes
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     */
    private $Notecc;

    /**
     * @ORM\Column(type="float")
     */
    private $Notetp;

    /**
     * @ORM\Column(type="float")
     */
    private $Noteexamen;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Etudiant", inversedBy="notes")
     */
    private $Etudiant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Matiere", inversedBy="notes")
     */
    private $Matiere;



    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Anneeuni", inversedBy="notes")
     */
    private $anneeuni;

    /**
     * @ORM\Column(type="float")
     */
    private $Moyenne;


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNotecc(): ?float
    {
        return $this->Notecc;
    }

    public function setNotecc(float $Notecc): self
    {
        $this->Notecc = $Notecc;

        return $this;
    }

    public function getNotetp(): ?float
    {
        return $this->Notetp;
    }

    public function setNotetp(float $Notetp): self
    {
        $this->Notetp = $Notetp;

        return $this;
    }

    public function getNoteexamen(): ?float
    {
        return $this->Noteexamen;
    }

    public function setNoteexamen(float $Noteexamen): self
    {
        $this->Noteexamen = $Noteexamen;

        return $this;
    }

    public function getEtudiant(): ?Etudiant
    {
        return $this->Etudiant;
    }

    public function setEtudiant(?Etudiant $Etudiant): self
    {
        $this->Etudiant = $Etudiant;

        return $this;
    }

    public function getMatiere(): ?Matiere
    {
        return $this->Matiere;
    }

    public function setMatiere(?Matiere $Matiere): self
    {
        $this->Matiere = $Matiere;

        return $this;
    }



    public function getAnneeuni(): ?Anneeuni
    {
        return $this->anneeuni;
    }

    public function setAnneeuni(?Anneeuni $anneeuni): self
    {
        $this->anneeuni = $anneeuni;

        return $this;
    }

    public function getMoyenne(): ?float
    {
        return $this->Moyenne;
    }

    public function setMoyenne(float $Moyenne): self
    {
        $this->Moyenne = $Moyenne;

        return $this;
    }


}
