<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

/**
 * @ORM\Entity(repositoryClass="App\Repository\SuperAdminRepository")
 */
class SuperAdmin extends Admin
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    protected $id;

    public function getId(): ?int
    {
        return $this->id;
    }
    /**
     * @ORM\Column(type="json")
     */
    protected $roles;


    /**
     * @inheritDoc
     */
    public function getRoles(): array
    {
        $roles = $this->roles;
        // guarantee every user at least has ROLE_USER
        $roles[] = 'ROLE_SUPER_ADMIN';

        return array_unique($roles);

    }

    /**
     * @param array $roles
     */
    public function setRoles($roles): void
    {
        $this->roles[] = $roles;
    }
    public function __toString()
    {
        return $this->getUsername();
        // TODO: Implement __toString() method.
    }
}
