<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\FormationRepository")
 */
class Formation
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Classe", mappedBy="formation")
     */
    private $Classe;


    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Niveau", inversedBy="Formation")
     */
    private $niveau;

    /**
     * @ORM\Column(type="integer")
     */
    private $prix;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $image;

    public function __construct()
    {
        $this->Classe = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return Collection|Classe[]
     */
    public function getClasse(): Collection
    {
        return $this->Classe;
    }

    public function addClasse(Classe $classe): self
    {
        if (!$this->Classe->contains($classe)) {
            $this->Classe[] = $classe;
            $classe->setFormation($this);
        }

        return $this;
    }

    public function removeClasse(Classe $classe): self
    {
        if ($this->Classe->contains($classe)) {
            $this->Classe->removeElement($classe);
            // set the owning side to null (unless already changed)
            if ($classe->getFormation() === $this) {
                $classe->setFormation(null);
            }
        }

        return $this;
    }
    public function __toString()
    {
        return $this->name;

    }





    public function getNiveau(): ?Niveau
    {
        return $this->niveau;
    }

    public function setNiveau(?Niveau $niveau): self
    {
        $this->niveau = $niveau;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }


    /**
     * @return mixed
     */
    public function getImage()
    {
        if ($this->image == "")
            return $this->image;
        else
            return 'uploads/' . basename($this->image);

    }

    /**
     * @param mixed $image
     */
    public function setImage($image): void
    {
        $this->image = basename($image);
    }




}
