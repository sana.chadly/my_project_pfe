<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\NiveauRepository")
 */
class Niveau
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_niveau;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Formation", mappedBy="niveau")
     */
    private $Formation;

    /**
     * @ORM\Column(type="integer")
     */
    private $Duree;

    public function __construct()
    {
        $this->Formation = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameNiveau(): ?string
    {
        return $this->name_niveau;
    }

    public function setNameNiveau(string $name_niveau): self
    {
        $this->name_niveau = $name_niveau;

        return $this;
    }

    /**
     * @return Collection|Formation[]
     */
    public function getFormation(): Collection
    {
        return $this->Formation;
    }

    public function addFormation(Formation $formation): self
    {
        if (!$this->Formation->contains($formation)) {
            $this->Formation[] = $formation;
            $formation->setNiveau($this);
        }

        return $this;
    }

    public function removeFormation(Formation $formation): self
    {
        if ($this->Formation->contains($formation)) {
            $this->Formation->removeElement($formation);
            // set the owning side to null (unless already changed)
            if ($formation->getNiveau() === $this) {
                $formation->setNiveau(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name_niveau;

    }

    public function getDuree(): ?int
    {
        return $this->Duree;
    }

    public function setDuree(int $Duree): self
    {
        $this->Duree = $Duree;

        return $this;
    }

}
