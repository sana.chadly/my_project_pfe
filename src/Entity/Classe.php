<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ClasseRepository")
 */
class Classe
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $name_classe;

    /**
     * @ORM\Column(type="integer")
     */
    private $nbre_etud;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Etudiant", mappedBy="classe")
     */
    private $etudiant;

    /**
     * @var \Doctrine\Common\Collections\ArrayCollection
     * @ORM\ManyToMany(targetEntity="App\Entity\Matiere", mappedBy="Classe")
     * @ORM\JoinTable(name="matiere_classe")
     */

    private $matieres;

    /**
     * @ORM\ManyToMany(targetEntity="App\Entity\Enseignant", inversedBy="classes")
     */
    private $Enseignant;

    /**
     * @ORM\ManyToOne(targetEntity="App\Entity\Formation", inversedBy="Classe")
     */
    private $formation;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Emploi", mappedBy="mploie")
     */
    private $emplois;



    public function __construct()
    {
        $this->etudiant = new ArrayCollection();
        $this->matieres = new ArrayCollection();
        $this->Enseignant = new ArrayCollection();
        $this->emplois = new ArrayCollection();

    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNameClasse(): ?string
    {
        return $this->name_classe;
    }

    public function setNameClasse(string $name_classe): self
    {
        $this->name_classe = $name_classe;

        return $this;
    }

    public function getNbreEtud(): ?int
    {
        return $this->nbre_etud;
    }

    public function setNbreEtud(int $nbre_etud): self
    {
        $this->nbre_etud = $nbre_etud;

        return $this;
    }

    /**
     * @return Collection|etudiant[]
     */
    public function getEtudiant(): Collection
    {
        return $this->etudiant;
    }

    public function addEtudiant(etudiant $etudiant): self
    {
        if (!$this->etudiant->contains($etudiant)) {
            $this->etudiant[] = $etudiant;
            $etudiant->setClasse($this);
        }

        return $this;
    }

    public function removeEtudiant(etudiant $etudiant): self
    {
        if ($this->etudiant->contains($etudiant)) {
            $this->etudiant->removeElement($etudiant);
            // set the owning side to null (unless already changed)
            if ($etudiant->getClasse() === $this) {
                $etudiant->setClasse(null);
            }
        }

        return $this;
    }

    public function __toString()
    {
        return $this->name_classe;

    }



    /**
     * @return Collection|Matiere[]
     */
    public function getMatieres(): Collection
    {
        return $this->matieres;
    }

    public function addMatiere(Matiere $matiere): self
    {
        if (!$this->matieres->contains($matiere)) {
            $this->matieres[] = $matiere;
            $matiere->addClasse($this);
        }

        return $this;
    }

    public function removeMatiere(Matiere $matiere): self
    {
        if ($this->matieres->contains($matiere)) {
            $this->matieres->removeElement($matiere);
            $matiere->removeClasse($this);
        }

        return $this;
    }

    /**
     * @return Collection|Enseignant[]
     */
    public function getEnseignant(): Collection
    {
        return $this->Enseignant;
    }

    public function addEnseignant(Enseignant $enseignant): self
    {
        if (!$this->Enseignant->contains($enseignant)) {
            $this->Enseignant[] = $enseignant;
        }

        return $this;
    }

    public function removeEnseignant(Enseignant $enseignant): self
    {
        if ($this->Enseignant->contains($enseignant)) {
            $this->Enseignant->removeElement($enseignant);
        }

        return $this;
    }

    public function getFormation(): ?Formation
    {
        return $this->formation;
    }

    public function setFormation(?Formation $formation): self
    {
        $this->formation = $formation;

        return $this;
    }

    /**
     * @return Collection|Emploi[]
     */
    public function getEmplois(): Collection
    {
        return $this->emplois;
    }

    public function addEmplois(Emploi $emplois): self
    {
        if (!$this->emplois->contains($emplois)) {
            $this->emplois[] = $emplois;
            $emplois->setMploie($this);
        }

        return $this;
    }

    public function removeEmplois(Emploi $emplois): self
    {
        if ($this->emplois->contains($emplois)) {
            $this->emplois->removeElement($emplois);
            // set the owning side to null (unless already changed)
            if ($emplois->getMploie() === $this) {
                $emplois->setMploie(null);
            }
        }

        return $this;
    }






}
