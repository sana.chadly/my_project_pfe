<?php

namespace App\DataFixtures;
use App\Entity\SuperAdmin;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;

class AppFixtures extends Fixture
{
    private  $encoder ;
    public function  __construct(UserPasswordEncoderInterface $encoder )
    {
        $this->encoder =  $encoder;
    }


    public function load(ObjectManager $manager)
    {

        $admin_repo=$manager->getRepository( SuperAdmin::class);
        $admin=$admin_repo->findOneBy(['roles'=>'ROLE_SUPER_ADMIN']);
        if(!$admin){

            $Admin = new  SuperAdmin();
            $Admin->setUsername("admin1")
                ->setEmail("admin1@admin.com");
            $pass=  $this->encoder->encodePassword($Admin, "123_pass");
            $Admin ->setPassword($pass);
            $Admin->setRoles('ROLE_SUPER_ADMIN');
            $Admin ->setDdn(\DateTime::createFromFormat('Y-m-d', "1999-09-09"));
            $Admin ->setNom("admin");
            $Admin ->setPrenom("admin");
            $Admin ->setCin("12828058");
            $Admin ->setCreatedAt(new \DateTime('now'));
            $Admin->setTel("22842632");
            $Admin->setAdress("sousse");
            $manager->persist($Admin);
        }
        $manager->flush();
    }



}

