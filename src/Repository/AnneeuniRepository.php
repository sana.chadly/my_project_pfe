<?php

namespace App\Repository;

use App\Entity\Anneeuni;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Anneeuni|null find($id, $lockMode = null, $lockVersion = null)
 * @method Anneeuni|null findOneBy(array $criteria, array $orderBy = null)
 * @method Anneeuni[]    findAll()
 * @method Anneeuni[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class AnneeuniRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Anneeuni::class);
    }

    // /**
    //  * @return Anneeuni[] Returns an array of Anneeuni objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('a.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Anneeuni
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
