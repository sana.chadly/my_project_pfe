<?php

namespace App\Repository;

use App\Entity\Evenement;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Common\Persistence\ManagerRegistry;

/**
 * @method Evenement|null find($id, $lockMode = null, $lockVersion = null)
 * @method Evenement|null findOneBy(array $criteria, array $orderBy = null)
 * @method Evenement[]    findAll()
 * @method Evenement[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class EvenementRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Evenement::class);
    }


    public function findNewEvents()
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.date_fin >= :val')
            ->setParameter('val', new \DateTime())
            ->getQuery()
            ->getResult()
        ;
    }
    public function findEntitiesByString($str)
    {
        return $this->createQueryBuilder('a')
            ->andWhere('a.nom like :str')
            ->setParameter('str','%'.$str.'%')
            ->andWhere('a.date_fin >= :val')
            ->setParameter('val', new \DateTime())
            ->getQuery()
            ->getResult();

    }

    /*
    public function findOneBySomeField($value): ?Evenement
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
