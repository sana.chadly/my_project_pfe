<?php

namespace App\Form;

use App\Entity\Anneeuni;
use App\Entity\Etudiant;
use App\Entity\Matiere;
use App\Entity\Notes;
use App\Entity\User;
use App\Repository\EnseignantRepository;
use App\Repository\MatiereRepository;
use Doctrine\ORM\EntityRepository;
use phpDocumentor\Reflection\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Security\Core\Security;

class NotesType extends AbstractType
{

    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Notecc')
            ->add('Notetp')
            ->add('Noteexamen')
            ->add('Etudiant', EntityType::class, [
                'class' => Etudiant::class,
                'choice_label' => 'username',
            ])
            ->add('Matiere', EntityType::class, [
                'class' => Matiere::class,
                'query_builder' => function (MatiereRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->innerJoin('u.enseignants', 's', 'WITH', 's.id = :user')
                        ->setParameter('user', $this->security->getUser());
                },
            ])
            ->add('anneeuni', EntityType::class, [
                'class' => Anneeuni::class,
                'choice_label' => 'Noman',
            ])
            ->add('valider', SubmitType::class, [
                'attr' => [
                    'class' => 'btn btn-default irs-big-btn'
                ]

            ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Notes::class,
            'user' => null
        ]);

    }
}
