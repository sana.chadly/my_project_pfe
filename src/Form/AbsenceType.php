<?php

namespace App\Form;

use App\Entity\Absence;
use App\Entity\Matiere;
use App\Repository\MatiereRepository;
use Doctrine\DBAL\Types\DateType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Security\Core\Security;

class AbsenceType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('Etat', ChoiceType::class,  [
                'choices'  => [
                    'Present' => 'Present',
                    'Absent' => 'Absent',
                ],
            ])
            ->add('date',\Symfony\Component\Form\Extension\Core\Type\DateType::class, [
                // renders it as a single text box
                'widget' => 'single_text',
                'input'  => 'string'

            ])
            ->add('relation', EntityType::class, [
                'class' => Matiere::class,
                'query_builder' => function (MatiereRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->innerJoin('u.enseignants', 's', 'WITH', 's.id = :user')
                        ->setParameter('user', $this->security->getUser());
                },
            ])
        ->add('valider', SubmitType::class, [
        'attr' => [
            'class' => 'btn btn-default irs-big-btn'
        ]

    ]);;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Absence::class,
        ]);
    }
}
