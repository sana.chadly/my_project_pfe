<?php

namespace App\Form;

use App\Entity\Classe;
use App\Entity\Cours;
use App\Entity\Matiere;
use App\Repository\MatiereRepository;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Security\Core\Security;



class CoursType extends AbstractType
{
    private $security;

    public function __construct(Security $security)
    {
        $this->security = $security;
    }
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name_cours')
            ->add('remarque')
            ->add('Matiere', EntityType::class, [
                'class' => Matiere::class,
                'query_builder' => function (MatiereRepository $er) {
                    return $er->createQueryBuilder('u')
                        ->innerJoin('u.enseignants', 's', 'WITH', 's.id = :user')
                        ->setParameter('user', $this->security->getUser());
                },
            ])

            ->add('brochure', FileType::class, [
                'label' => 'PDF file',
                'mapped' => false,


            ])
            ->add('valider', SubmitType::class);
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Cours::class,
        ]);
    }
}
